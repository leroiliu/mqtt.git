<?php


namespace Leroi\mqtt;
use Leroi\StaticInstance\StaticInstance;

class MQTT
{

    use StaticInstance;


    public function connect(&$mqtt,$client_id,$config=array())
    {
        $server = $config['server']??'127.0.0.1';
        $port   = $config['port']??1883;
        $username   = $config['username']??'pkk';
        $password   = $config['password']??'pkk';
        $mqtt = new phpMQTT($server,$port,$client_id);
        if ($mqtt->connect(true, NULL,$username,$password)){
            return true;
        }
        return false;
    }

    public function uuid_create($data = array()){
        return md5(serialize($data).uniqid('',true));
    }


}